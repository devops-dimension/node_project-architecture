cat << EOF
   _    _      _ _
  | |  | |    | | |
  | |__| | ___| | | ___
  |  __  |/ _ \ | |/ _ \
  | |  | |  __/ | | (_) |
  |_|  |_|\___|_|_|\___/

EOF
# build project
base=$(basename $PWD)
cd ..

tar -czf ${BUILD_TAG}-${GIT_COMMIT}.tar.gz --exclude='.git' $base

mkdir -p $base/build/dist/
mv ${BUILD_TAG}-${GIT_COMMIT}.tar.gz $base/build/dist/
