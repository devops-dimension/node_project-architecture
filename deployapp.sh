#!/bin/bash
ssh 'ubuntu@ec2' << EOF
    df -h / | tee >(cat /etc/os-release) >(free -h) | cowsay -f duck | lolcat
    cowsay -f tux HELLO | lolcat
EOF
sleep 5
# variables
BASE=/var/www/backend
# copy artifact
rsync -e ssh -arvc ./build/dist/${BUILD_TAG}-${GIT_COMMIT}.tar.gz ubuntu@ec2:$BASE/release

# connection remout host
connect_ssh() {
    ssh ubuntu@ec2 $1
}
connect_ssh "cd $BASE/release && mkdir ${BUILD_TAG}-${GIT_COMMIT}"
connect_ssh "cd $BASE/release && tar xf ${BUILD_TAG}-${GIT_COMMIT}.tar.gz -C ${BUILD_TAG}-${GIT_COMMIT} --strip-components 1"
connect_ssh "find $BASE/release -name '*.tar.gz' -delete"
connect_ssh "rm -rf $BASE/current && ln -sf $BASE/release/${BUILD_TAG}-${GIT_COMMIT}/ $BASE/current"
connect_ssh "cd $BASE/current && mkdir dhparam"
connect_ssh "sudo openssl dhparam -out $BASE/current/dhparam/dhparam-2048.pem 2048"